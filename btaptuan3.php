<!DOCTYPE html>
<html>
<head>
  <style>
    /* CSS for label and button */
    label {
      color: blue; /* Change the color as needed */
      font-family: Arial, sans-serif; /* Change the font-family as needed */
    }

    button {
      color: green; /* Change the color as needed */
      font-family: Arial, sans-serif; /* Change the font-family as needed */
    }
  </style>
</head>
<body>
  <h2>Đăng ký tân sinh viên</h2>
  <form action="process_registration.php" method="post">
    <label for="name">Họ và tên:</label>
    <input type="text" id="name" name="name" required><br><br>

    <label for="gender">Giới tính:</label>
    <select id="gender" name="gender">
      <option value="">--Chọn giới tính--</option>
      <option value="0">Nam</option>
      <option value="1">Nữ</option>
    </select><br><br>

    <label for="department">Phân khoa:</label>
    <select id="department" name="department">
      <option value="">--Chọn phân khoa--</option>
      <option value="MAT">Khoa học máy tính</option>
      <option value="KDL">Khoa học vật liệu</option>
    </select><br><br>

    <button type="submit">Đăng ký</button>
  </form>
</body>
</html>